import properties from "src/configs/properties"

export const defaultState = {
  selectedStyle: "e9a6af0d5d31436ab405fc88d0210f73",
  tableSelected: false,
  traySelected: false,
  selectedTray: {
    title: "Oval",
    id: 1,
    param3d: "table_oval"
  }
}

const state = {
  errorSummary: {
    code: 500,
    message: "General Error"
  },
  loaded: false,
  currentStep: 1,
  selectedStyle: "e9a6af0d5d31436ab405fc88d0210f73",
  selectedStyleForCommit: null,
  selectedLayout: 1,
  selectedColor: {
    1: "#7B0016",
    2: "#090909"
  },
  selectedCupHolder: 1,
  selectedWoodTrim: "Black Walnut",
  selectedTray: {
    title: "Oval",
    id: 1,
    param3d: "table_oval"
  },
  quantity: 1,
  layoutText: "Layout 1",
  styleText: "Home Theater Sectional",
  colorText: {
    1: "Theater Red",
    2: "Black Onyx"
  },
  materialText: {
    1: "Cine Suede",
    2: "Silk Leather"
  },
  cupHolderText: "Black",
  expandedMobileControl: false,
  tableSelected: false,
  displayExportDialog: false,
  displayContactDialog: false,
  displayErrorDialog: false
}

const mutations = {
  GOTO_STEP (state, index) {
    state.currentStep = index
  },
  SET_LOADING (state) {
    state.loaded = false
  },
  SET_LOADED (state) {
    state.loaded = true
  },
  SET_LAYOUT (state, { id, text }) {
    state.selectedLayout = id
    state.layoutText = text
  },
  SET_STYLE (state, { id, text }) {
    state.selectedStyle = id
    state.styleText = text
  },
  SET_STYLE_FOR_COMMIT (state, id) {
    state.selectedStyleForCommit = id
  },
  SET_COLOR (state, { color, text, material, section }) {
    state.selectedColor[section] = color
    state.colorText[section] = text
    state.materialText[section] = material
  },
  SET_CUP_HOLDER (state, { id, text }) {
    state.selectedCupHolder = id
    state.cupHolderText = text
  },
  SET_WOOD_TRIM (state, { name, text }) {
    state.selectedWoodTrim = name
  },
  SET_QUANTITY (state, num) {
    state.quantity = num
  },
  TOGGLE_MOBILE_CONTROL (state) {
    state.expandedMobileControl = !state.expandedMobileControl
  },
  SET_TABLE (state, selected) {
    state.tableSelected = selected
  },
  SET_TRAY (state, tray) {
    state.selectedTray = tray
  },
  SHOW_EXPORT_DIALOG (state) {
    state.displayExportDialog = true
  },
  CLOSE_EXPORT_DIALOG (state) {
    state.displayExportDialog = false
  },
  SHOW_CONTACT_DIALOG (state) {
    state.displayContactDialog = true
  },
  CLOSE_CONTACT_DIALOG (state) {
    state.displayContactDialog = false
  },
  SHOW_ERROR_DIALOG (state) {
    state.displayErrorDialog = true
  },
  CLOSE_ERROR_DIALOG (state) {
    state.displayErrorDialog = false
  },
  SET_ERROR (state, { error }) {
    state.errorSummary = {
      ...state.errorSummary,
      ...error
    }
  }
}

const actions = {
  setLoading ({ commit }) {
    commit("SET_LOADING")
  },
  setLoaded ({ commit }) {
    commit("SET_LOADED")
  },
  gotoStep ({ commit }, id) {
    commit("GOTO_STEP", id)
  },
  nextStep ({ commit }) {
    if (state.currentStep < Object.keys(properties).length) {
      commit("GOTO_STEP", state.currentStep + 1)
    }
  },
  prevStep ({ commit }) {
    if (state.currentStep > 1) commit("GOTO_STEP", state.currentStep - 1)
  },
  selectLayout ({ commit }, layout) {
    commit("SET_LAYOUT", layout)
  },
  selectStyle ({ commit }, style) {
    commit("SET_STYLE", style)
  },
  selectStyleForCommit ({ commit }, id) {
    commit("SET_STYLE_FOR_COMMIT", id)
  },
  selectColor ({ commit }, color) {
    commit("SET_COLOR", color)
  },
  selectCupHolder ({ commit }, cupholder) {
    commit("SET_CUP_HOLDER", cupholder)
  },
  selectWoodTrim ({ commit }, woodtrim) {
    commit("SET_WOOD_TRIM", woodtrim)
  },
  setQuantity ({ commit }, num) {
    commit("SET_QUANTITY", num)
  },
  toggleMobileControl ({ commit }) {
    commit("TOGGLE_MOBILE_CONTROL")
  },
  setTable ({ commit }, selected) {
    commit("SET_TABLE", selected)
  },
  setTray ({ commit }, tray) {
    commit("SET_TRAY", tray)
  },
  showExportDialog ({ commit }) {
    commit("SHOW_EXPORT_DIALOG")
  },
  closeExportDialog ({ commit }) {
    commit("CLOSE_EXPORT_DIALOG")
  },
  showContactDialog ({ commit }) {
    commit("SHOW_CONTACT_DIALOG")
  },
  closeContactDialog ({ commit }) {
    commit("CLOSE_CONTACT_DIALOG")
  }
}
const getters = {
  lastStep () {
    return state.currentStep >= Object.keys(properties).length
  },
  firstStep () {
    return state.currentStep === 1
  },
  getCheque () {
    return {
      layoutText: state.layoutText,
      styleText: state.styleText,
      colorText: state.colorText,
      materialText: state.materialText,
      cupHolderText: state.cupHolderText,
      woodtrimText: state.selectedWoodTrim,
      quantity: state.quantity,
      table: state.tableSelected
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
