export default class ErrorService {
  constructor (store) {
    this.store = store
    this.initHandler()
  }

  onError (message) {
    // Send Error to Log Engine e.g LogRocket
    this.logRocketLogEngine(message)
    console.error("onErrorHandler: ", message)
    this.store.commit("app/SET_ERROR", {
      error: { message }
    })
    this.store.commit("app/SHOW_ERROR_DIALOG")
  }

  onWarn (error) {
    // Send Error to Log Engine e.g LogRocket
    this.logRocketLogEngine(error)
  }

  onInfo (error) {
    // You can handle this differently
    this.sentryLogEngine(error)
  }

  onDebug (error) {
    // Send Error to Log Engine e.g LogRocket
    this.logRocketLogEngine(error)
  }

  initHandler () {
    const scope = this
    window.onerror = (message, url, lineNo, columnNo, error) => {
      console.error(error, "window onerror")
      if (error) {
        scope.onError(error)
        console.log(message, url, lineNo, columnNo, error)
      }
    }
  }

  logRocketLogEngine (error) {
    // Implement LogRocket Engine here
    console.log("submit error to logrocket", error)
  }
}
