import Email from "src/services/smtp-tapmod"
// const apiEndpoint = "https://api.tapmod.studio/.netlify/functions/sendgrid" // Tapmod LAMBDA API
const apiEndpoint = "http://localhost:3000/msg"

export default function sendEmail (data, cb) {
  const msg = {
    Profile: "elite-hts",
    Fields: {
      ...data
    }
  }

  // eslint-disable-next-line camelcase
  const msg_manager = {
    ...JSON.parse(JSON.stringify(msg)),
    To: "sales@elitehts.com",
    // To: "sunman.le@gmail.com",
    Reply_to: data.email
  }
  // eslint-disable-next-line camelcase
  const msg_customer = {
    ...JSON.parse(JSON.stringify(msg)),
    To: data.email
  }
  msg_customer.Fields.isCustomer = true
  return Promise.all([
    () => Email.send(msg_manager),
    () => Email.send(msg_customer)
  ])
}
