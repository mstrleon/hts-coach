import Axios from "axios"

// const apiEndpoint = "https://api.tapmod.studio/.netlify/functions/sendgrid" // Tapmod LAMBDA API
const apiEndpoint = "http://localhost:3000/msg" // Local
const Email = {
  send (message) {
    return Axios.post(apiEndpoint, message)
  }
}

export default Email
