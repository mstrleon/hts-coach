const scrollStyle = {
  thumb: {
    right: '2px',
    borderRadius: '5px',
    backgroundColor: '#af8562',
    width: '4px',
    height: '4px',
    opacity: 0.75
  },
  bar: {
    right: '1px',
    borderRadius: '9px',
    backgroundColor: '#af8562',
    width: '4px',
    opacity: 0
  },
}

export default scrollStyle
