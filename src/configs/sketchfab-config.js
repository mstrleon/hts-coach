export default {
  // callback_onload: product_init,
  // "onload_callback": false,
  elementSelectors: {
    iframe: "#api-frame",
    loading: "#loading",
    annotation: "#annotation-button",
    config_container: ".configurator-container .configurator"
  },
  options: {
    camera: 0,
    autostart: 1,
    autospin: 0, // 0.1
    preload: 0,
    dof_circle: 0,
    annotations_visible: 0,
    animation_autoplay: 1,
    ui_infos: 0,
    ui_annotations: 0,
    ui_controls: 0,
    ui_stop: 0,
    ui_help: 0,
    ui_hint: 1,
    ui_inspector: 0,
    ui_watermark: 0,
    scrollwheel: 1,
    double_click: 0,
    transparent: 0,
    merge_materials: 1,
    graph_optimizer: 0
    // orbit_constraint_pan: 1,
    // orbit_constraint_zoom_in: 20,
    // orbit_constraint_zoom_out: 36,
    // "orbit_constraint_pitch_down" : 0.0
  }
}
