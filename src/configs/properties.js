const properties = {
  step1: {
    left: {
      header: {
        title: "Choose your style"
      },
      itemGroups: {
        1: {
          component: "StyleTypeSelector",
          mobileWidth: "130px",
          items: {
            1: {
              id: 1,
              title: "Home Theater Sectional",
              thumbnail: "icons/theater-sectional.jpg",
              uid: "e9a6af0d5d31436ab405fc88d0210f73"
            },
            2: {
              id: 2,
              title: "Cuddle Couch",
              thumbnail: "icons/cuddle-couch.jpg",
              uid: "2bc87c7b40034b5396dc700e0bf0a3cc"
            },
            3: {
              id: 3,
              title: "Theater Bed",
              thumbnail: "icons/theater-bed.jpg",
              uid: "8b712bb78aed4a72bfe155858ad9d031"
            }
          }
        }
      }
    },
    right: {
      header: {
        title: "Choose your layout"
      },
      itemGroups: {
        1: {
          component: "LayoutSelector",
          items: {
            1: {
              id: 1,
              title: "Layout 1",
              thumbnail: "icons/layout-1-1.png",
              node: "layout_1",
              annotation: "layout_1",
              uid: "e9a6af0d5d31436ab405fc88d0210f73"
            },
            2: {
              id: 2,
              title: "Layout 2",
              thumbnail: "icons/layout-1-2.png",
              node: "layout_2",
              annotation: "layout_2",
              uid: "e9a6af0d5d31436ab405fc88d0210f73"
            },
            3: {
              id: 3,
              title: "Layout 3",
              thumbnail: "icons/layout-1-3.png",
              node: "layout_3",
              annotation: "layout_3",
              uid: "e9a6af0d5d31436ab405fc88d0210f73"
            },
            4: {
              id: 4,
              title: "Layout 4",
              thumbnail: "icons/layout-1-4.png",
              node: "layout_4",
              annotation: "layout_4",
              uid: "e9a6af0d5d31436ab405fc88d0210f73"
            },
            5: {
              id: 5,
              title: "Standard",
              thumbnail: "icons/layout-2-1.png",
              node: "layout1",
              annotation: "layout_1",
              uid: "2bc87c7b40034b5396dc700e0bf0a3cc"
            },
            6: {
              id: 6,
              title: "Extra Wide",
              thumbnail: "icons/layout-2-2.png",
              node: "layout2",
              annotation: "layout_2",
              uid: "2bc87c7b40034b5396dc700e0bf0a3cc"
            },
            7: {
              id: 7,
              title: "Custom Width",
              thumbnail: "icons/layout-2-3.png",
              node: "layout3",
              annotation: "layout_3",
              uid: "2bc87c7b40034b5396dc700e0bf0a3cc"
            },
            8: {
              id: 8,
              title: "Single",
              thumbnail: "icons/layout-3-1.png",
              node: "layout1",
              annotation: "layout_1",
              uid: "8b712bb78aed4a72bfe155858ad9d031"
            },
            9: {
              id: 9,
              title: "Double",
              thumbnail: "icons/layout-3-2.png",
              node: "layout2",
              annotation: "layout_2",
              uid: "8b712bb78aed4a72bfe155858ad9d031"
            },
            10: {
              id: 10,
              title: "Triple",
              thumbnail: "icons/layout-3-3.png",
              node: "layout3",
              annotation: "layout_3",
              uid: "8b712bb78aed4a72bfe155858ad9d031"
            },
            11: {
              id: 11,
              title: "Triple Wide",
              thumbnail: "icons/layout-3-4.png",
              node: "layout4",
              annotation: "layout_4",
              uid: "8b712bb78aed4a72bfe155858ad9d031"
            }
          }
        }
      }
    }
  },
  step2: {
    left: {
      header: {
        title: "Choose color for section 1",
        center: true
      },
      itemGroups: {
        1: {
          header: {
            title: "Silk Leather",
            subtitle: "Custom colors available upon request",
            tooltip:
              "Our most popular material - an extremely buttery soft, breathable, and flexible synthetic material that has the visual and spill resistant qualities of leather. This premium material carries a 10% surcharge over Valentino Leather.",
            image: "icons/step2/silk-leather.png"
          },
          component: "ColorSelector",
          section: 1,
          mobilePanel: 1,
          param3d: {
            material: "cloth_1_mat",
            specular: 0.3,
            glossiness: 0.8,
            normal: "silk_leather_normal_0.jpg",
            cavity: "silk_leather_cavity_0.jpg",
            metalnessFactor: 0
          },
          items: {
            Ash: "#47433e",
            Aztec: "#61331a",
            "Beet Root": "#431e1f",
            "Black Onyx": "#090909",
            Cabernet: "#281312",
            "French Vanilla": "#978264",
            Caramel: "#57301d",
            "Desert Clay": "#84654f",
            Indigo: "#1a1b2d",
            Truffle: "#453329",
            "Evening Blue": "#080e20",
            "Pompeian Red": "#5b0401",
            Shiitake: "#413735",
            Peppercorn: "#1e1513"
          }
        },
        2: {
          header: {
            title: "Cine Suede",
            subtitle: "Custom colors available upon request",
            tooltip:
              "An extremely soft, high quality suede-textured fabric that provides a warm and cozy, true-to-theater cinematic experience.",
            image: "icons/step2/cine-suede.png"
          },
          component: "ColorSelector",
          section: 1,
          MobilePanel: 1,
          param3d: {
            material: "cloth_1_mat",
            specular: 1.0,
            glossiness: 0.38,
            normal: "suede_n.jpg",
            metalnessFactor: 0.75
          },
          items: {
            Platinum: "#aca497",
            Bordeaux: "#330f0f",
            Chocolate: "#21120d",
            Grape: "#242c2f",
            Navy: "#1a1b2d",
            Graphite: "#242c2f",
            Midnight: "#0e0e0e",
            Rust: "#420d07",
            Saddle: "#372116",
            Taupe: "#867058"
          }
        },
        3: {
          header: {
            id: "cupholder",
            title: "Choose Your Cupholder Finish",
            center: false,
            useSwitch: false
          },
          component: "CupHolderSelector",
          mobilePanel: 2,
          mobileHeight: "190px",
          param3d: "",
          items: {
            1: {
              id: 1,
              title: "Black",
              param3d: {
                material: "cupholders_mat",
                roughness: 0.07,
                metalness: 0,
                color: "#242424"
              },
              thumbnail: "icons/step2/cup_black.png"
            },
            2: {
              id: 2,
              param3d: {
                material: "cupholders_mat",
                roughness: 0.24,
                metalness: 1,
                color: "#5F5E5A"
              },
              title: "Gun Metal",
              thumbnail: "icons/step2/cup_gun_metal.png"
            },
            3: {
              id: 3,
              title: "Copper",
              param3d: {
                material: "cupholders_mat",
                roughness: 0.24,
                metalness: 1,
                color: "#6A5E4B"
              },
              thumbnail: "icons/step2/cup_copper.png"
            },
            4: {
              id: 4,
              title: "Silver",
              param3d: {
                material: "cupholders_mat",
                roughness: 0.4,
                metalness: 1,
                color: "#B6BBBD"
              },
              thumbnail: "icons/step2/cup_silver.png"
            }
          }
        }
      }
    },
    right: {
      header: {
        title: "Choose color for section 2",
        center: true
      },
      itemGroups: {
        1: {
          header: {
            title: "Silk Leather",
            subtitle: "Custom colors available upon request",
            tooltip:
              "Our most popular material - an extremely buttery soft, breathable, and flexible synthetic material that has the visual and spill resistant qualities of leather. This premium material carries a 10% surcharge over Valentino Leather.",
            image: "icons/step2/silk-leather.png"
          },
          section: 2,
          mobilePanel: 1,
          component: "ColorSelector",
          param3d: {
            material: "cloth_2_mat",
            specular: 0.3,
            glossiness: 0.8,
            normal: "silk_leather_normal_0.jpg",
            cavity: "silk_leather_cavity_0.jpg",
            metalnessFactor: 0
          },
          items: {
            Ash: "#47433e",
            Aztec: "#61331a",
            "Beet Root": "#431e1f",
            "Black Onyx": "#090909",
            Cabernet: "#281312",
            "French Vanilla": "#978264",
            Caramel: "#57301d",
            "Desert Clay": "#84654f",
            Indigo: "#1a1b2d",
            Truffle: "#453329",
            "Evening Blue": "#080e20",
            "Pompeian Red": "#5b0401",
            Shiitake: "#413735",
            Peppercorn: "#1e1513"
          }
        },
        2: {
          header: {
            title: "Cine Suede",
            subtitle: "Custom colors available upon request",
            tooltip:
              "An extremely soft, high quality suede-textured fabric that provides a warm and cozy, true-to-theater cinematic experience.",
            image: "icons/step2/cine-suede.png"
          },
          component: "ColorSelector",
          section: 2,
          mobilePanel: 1,
          param3d: {
            material: "cloth_2_mat",
            specular: 1.0,
            glossiness: 0.38,
            normal: "suede_n.jpg",
            metalnessFactor: 0.75
          },
          items: {
            Platinum: "#aca497",
            Bordeaux: "#330f0f",
            Chocolate: "#21120d",
            Grape: "#242c2f",
            Navy: "#1a1b2d",
            Graphite: "#242c2f",
            Midnight: "#0e0e0e",
            Rust: "#420d07",
            Saddle: "#372116",
            Taupe: "#867058"
          }
        },
        3: {
          uid: "e9a6af0d5d31436ab405fc88d0210f73",
          header: {
            title: "Snack Table",
            subtitle: "Choose Your Woodtrim",
            center: true,
            useSwitch: true,
            selected: false
          },
          demoImage: "icons/step2/snack_table.png",
          component: "WoodTrimSelector",
          param3d: "",
          mobilePanel: 2,
          items: {
            white_oak: {
              name: "White Oak",
              textureName: "wood.png",
              hex: "#EDB6A7"
            },
            chestnut: {
              name: "Chestnut",
              textureName: "wood.png",
              hex: "#E09563"
            },
            october_oak: {
              name: "October Oak",
              textureName: "wood.png",
              hex: "#E28662"
            },
            cinnamon: {
              name: "Cinnamon",
              textureName: "wood.png",
              hex: "#A46952"
            },
            black_walnut: {
              name: "Black Walnut",
              textureName: "wood.png",
              hex: "#805942"
            },
            mahogany: {
              name: "Mahogany",
              textureName: "wood.png",
              hex: "#884848"
            },
            charcoal: {
              name: "Charcoal",
              textureName: "wood.png",
              hex: "#725C4F"
            },
            piano_black: {
              name: "Piano Black",
              textureName: "wood.png",
              hex: "#3C3C3C"
            }
          }
        },
        4: {
          uid: "2bc87c7b40034b5396dc700e0bf0a3cc",
          header: {
            title: "Swivel Tray",
            center: true,
            useSwitch: true,
            selected: false
          },
          mobilePanel: 2,
          component: "TraySelector",
          items: {
            1: {
              id: 1,
              title: "Oval",
              thumbnail: "icons/step2/table_oval.png",
              param3d: "table_oval"
            },
            2: {
              id: 2,
              title: "Rectangular",
              thumbnail: "icons/step2/table_rect.png",
              param3d: "table_rectangular"
            }
          }
        },
        5: {
          uid: "8b712bb78aed4a72bfe155858ad9d031",
          header: {
            title: "Swivel Tray",
            center: true,
            useSwitch: true,
            selected: false
          },
          mobilePanel: 2,
          component: "TraySelector",
          items: {
            1: {
              id: 1,
              title: "Oval",
              thumbnail: "icons/step2/table_oval.png",
              param3d: "table_oval"
            },
            2: {
              id: 2,
              title: "Rectangular",
              thumbnail: "icons/step2/table_rect.png",
              param3d: "table_rectangular"
            }
          }
        }
      }
    }
  },
  step3: {
    left: {
      itemGroups: {
        1: {
          mobileHeight: "0px",
          component: "YouHaveChosen",
          items: {
            1: {}
          }
        }
      }
    },
    right: {
      itemGroups: {
        1: {
          mobileHeight: "300px",
          component: "FormSubmit",
          items: {
            1: {
              text:
                "You have selected a custom configuration which is hand made in Canada and comes with an industry leading 20 Year Warranty. <br><br> Please allow 1 business day for price quotation. For immediate pricing requests, please call our concierge/sales team at 604-575-8310 (8am to 4pm PST). International residents may also call 0141 445 4195 (8:30am to 4:30pm GMT/UTC) <br><br>Please note, retail pricing starts at $2950 MSRP. Wholesale pricing available to registered home professionals including Theater Installers, Interior Designers, Home Builders and Design Firms."
            }
          }
        }
      }
    }
  }
}

export default properties
