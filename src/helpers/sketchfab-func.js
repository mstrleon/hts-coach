import SketchfabViewApiWrapper from "sketchfab-api-wrapper"
import SFConfig from "../configs/sketchfab-config"

let sf // SketchFab API

export default async function SFInit (uid, exportTransparent) {
  const { options, elementSelectors } = SFConfig
  const iframeQuery = elementSelectors.iframe

  if (exportTransparent) SFConfig.options.transparent = 1
  let api = {}
  try {
    api = new SketchfabViewApiWrapper({
      iframeQuery,
      uid,
      options,
      version: "1.9.0",
      useSingleton: false
    })
  } catch (e) {
    throw new Error("Could not create SketchFab API instance")
  }
  try {
    await api.init()
  } catch (e) {
    throw new Error("Could not init SketchFab API")
  }
  window.sf = api // for manual testing in devtools
  sf = api
  return api // for unit testing
}

export function switchLayout (group, currentUid, item) {
  hideLayout(group, currentUid)

  item =
    item || Object.values(group.items).find(item => item.uid === currentUid)

  loadLayout(item)
}

function hideLayout (group, currentUid) {
  Object.values(group.items).forEach(item => {
    if (item.uid !== currentUid) {
      return
    }
    try {
      if (!item.node) throw Error()
      sf.hideNode(item.node)
    } catch (e) {
      throw new Error(
        "Could not hide the appropriate node for the given layout " + e
      )
    }
  })
}

function loadLayout (item) {
  try {
    sf.showNode(item.node)
  } catch (error) {
    throw new Error("Could not hide node " + error)
  }
  sf.gotoAnnotation(sf._getAnnotationByName(item.annotation).index)
}

export function switchColor (hexColor, param3d) {
  if (
    !hexColor ||
    !param3d ||
    !param3d.specular ||
    !param3d.glossiness // ||
    //! param3d.metalnessFactor // unnesecary
  ) {
    throw new Error("bad switch color parameters given")
  }

  const linerColor = sf.hexToLiner(hexColor)
  const mat = sf._getMaterialByName(param3d.material)

  mat.channels.SpecularF0.factor = param3d.specular
  mat.channels.AlbedoPBR.color = linerColor
  mat.channels.GlossinessPBR.factor = param3d.glossiness
  mat.channels.GlossinessPBR.enable = true
  mat.channels.CavityPBR.enable = !!param3d.cavity
  mat.channels.NormalMap.enable = true
  mat.channels.MetalnessPBR.factor = param3d.metalnessFactor

  mat.channels.NormalMap.texture =
    sf.textures.find(t => t.name === param3d.normal) || []
  mat.channels.CavityPBR.texture =
    sf.textures.find(t => t.name === param3d.cavity) || []
  try {
    sf.setMaterial(mat)
  } catch (e) {
    throw Error("could not apply new material " + e)
  }
}

export function switchColorOnCup (param3d) {
  const linerColor = sf.hexToLiner(param3d.color)
  const mat = sf._getMaterialByName(param3d.material)

  mat.channels.AlbedoPBR.color = linerColor
  mat.channels.MetalnessPBR.factor = param3d.metalness
  mat.channels.RoughnessPBR.enable = param3d.roughness
  try {
    sf.setMaterial(mat)
  } catch (e) {
    throw new Error("could not apply new material " + e)
  }
}

export function switchWood (hexColor) {
  const linerColor = sf.hexToLiner(hexColor)
  const mat = sf._getMaterialByName("wood_mat")
  mat.channels.AlbedoPBR.color = linerColor
  try {
    sf.setMaterial(mat)
  } catch (e) {
    throw new Error("could not apply new material " + e)
  }
}

export function showTable (show) {
  if (show) {
    try {
      sf.showNode("table")
    } catch (error) {
      throw new Error("cannot find table node")
    }
  } else {
    try {
      sf.hideNode("table")
    } catch (error) {
      throw new Error("Cannot find table node" + error)
    }
  }
}

export function switchTray (param3d) {
  try {
    sf.hideNode("table_oval")
    sf.hideNode("table_rectangular")
    sf.showNode(param3d)
  } catch (error) {
    throw new Error("Error on showing tray " + error)
  }
}

export const exportedForTesting = {
  loadLayout,
  hideLayout
}
