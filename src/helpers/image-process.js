export default function trimImage (img, posX, posY, recWidth, recHeight) {
  const canvas = document.createElement("canvas")
  canvas.width = recWidth
  canvas.height = recHeight
  const context = canvas.getContext("2d")
  context.drawImage(img, -posX, -posY)
  return canvas.toDataURL()
}
