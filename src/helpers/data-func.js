import properties from 'src/configs/properties'

export default function getDefaultLayoutForStyle (uid) {
  return Object.values(properties.step1.right.itemGroups[1].items)
    .find(layout => layout.uid === uid)
}

export function getStyleById (id) {
  return properties.step1.left.itemGroups[1].items[id]
}
