// import something here
import ErrorService from "src/services/error-service"
// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/boot-files
export default ({ Vue, store }) => {
  const errorService = new ErrorService(store)
  Vue.config.errorHandler = error => {
    console.warn(error)
    errorService.onError(error)
  }
  Vue.prototype.$errorService = errorService
}
