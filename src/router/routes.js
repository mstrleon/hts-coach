const routes = [

  {
    path: '/style/:styleTypeId',
    component: () => import('layouts/MainLayout.vue'),
    props: true,
    children: [
      { path: '', component: () => import('pages/Index.vue') },
    ],
  },
  { path: '/', redirect: '/style/1' },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('src/pages/PageError404.vue'),
  },
]

export default routes
