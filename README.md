<img src="public/eliteHTS.png" alt="Logo" width="220">


# Elite HTS Enterprise 3D Configurator

A Vue version of [EliteHTS](https://elitehts.tapmod.studio/) configurator with new design and new 3D models. 





## Features

- Works both on mobile and desktop platfroms
- Changing colors of meshes and switching 3dnodes
- Three different scenes
- Automatic config generation
- Email configuration and contact form
- Beautiful loader


## Web application live:

![Demo](public/hts-coach.gif)



https://hts-coach.tapmod.studio/


## Installation

#### Just clone the project and use Yarn to install
```bash
yarn

```

#### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev

```

#### Lint the files
```bash
yarn run lint

```

#### Build the app for production
```bash
quasar build

```


###     
## Used By

This project is used by:

<a href="http://elitehts.com" target="_blank">
<img src="public/logo_revised_v2.png" alt="ELITE-HTS" width="180">
</a>


## Author

- [Leonid Anufriev](https://www.gitlab.com/mstrleon)


## Tech Stack

**Client:** Vue, Quasar

**Server:** Netlify Functions

**3d Model:** SketchFab

