const dummyMat = {
  channels: {
    AlbedoPBR: {},
    GlossinessPBR: {},
    SpecularF0: {},
    CavityPBR: {},
    NormalMap: {},
    MetalnessPBR: {},
    RoughnessPBR: {}
  }
}

const mock = jest.fn().mockImplementation(({ uid }) => {
  return {
    init: jest.fn(),
    uid,
    hideNode: jest.fn(),
    showNode: jest.fn(),
    _getAnnotationByName: jest.fn(val => {
      return { index: +val || 1 }
    }),
    _getMaterialByName: jest.fn(() => {
      return dummyMat
    }),
    setMaterial: jest.fn(() => {}),
    gotoAnnotation: jest.fn(),
    hexToLiner: jest.fn(() => 777),
    textures: []
  }
})
export default mock
