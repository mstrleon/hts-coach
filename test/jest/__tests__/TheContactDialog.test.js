import { mount, createLocalVue } from "@vue/test-utils"
import TheContactDialog from "components/Dialogs/TheContactDialog"
import * as All from "quasar"
const { Quasar } = All
import Vuex from "vuex"

const components = Object.keys(All).reduce((object, key) => {
  const val = All[key]
  if (val && val.component && val.component.name != null) {
    object[key] = val
  }
  return object
}, {})

const localVue = createLocalVue()
localVue.use(Quasar, { components, directives: { ClosePopup: All.ClosePopup } })
localVue.use(Vuex)

const app = {
  namespaced: true,
  state: () => ({
    displayContactDialog: false
  }),
  mutations: {
    SHOW_CONTACT_DIALOG (state) {
      state.displayContactDialog = true
    },
    CLOSE_CONTACT_DIALOG (state) {
      state.displayContactDialog = false
    }
  }
}

function createStore (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      app
    }
  })

  return Store
}

function factory () {
  const store = createStore()
  return mount(TheContactDialog, {
    store,
    localVue
  })
}

describe("The Contact Dialog", () => {
  it("Updates Qdialog value to true on vuex change", async () => {
    const wrapper = factory()
    const vm = wrapper.vm
    vm.$store.commit("app/SHOW_CONTACT_DIALOG")
    const QDialog = wrapper.findComponent({ name: "QDialog" })
    await localVue.nextTick()

    expect(QDialog.props().value).toBe(true)
  })

  it("Displays a proper contact message", async () => {
    const wrapper = factory()
    const vm = wrapper.vm
    vm.$store.commit("app/SHOW_CONTACT_DIALOG")
    await localVue.nextTick()
    const contactMessage = wrapper.findComponent({ ref: "contactMessage" })

    expect(contactMessage.exists()).toBe(true)
    expect(contactMessage.html()).toContain("sales@elitehts.com")
    expect(contactMessage.html()).toContain("604-575-8310")
    expect(contactMessage.html()).toContain("0141 445 4195")
  })

  it("Has a close button and it works", async () => {
    const wrapper = factory()
    const vm = wrapper.vm
    vm.$store.commit("app/SHOW_CONTACT_DIALOG")
    await localVue.nextTick()

    const closeButton = wrapper.findComponent({ ref: "close" })
    expect(closeButton.exists()).toBe(true)

    closeButton.trigger("hide")
    await localVue.nextTick()
    const QDialog = wrapper.findComponent({ name: "QDialog" })

    expect(QDialog.props().value).toBe(false)
    expect(vm.$store.state.app.displayContactDialog).toBe(false)
  })

  it("Updates vuex on hide event", async () => {
    const wrapper = factory()
    const vm = wrapper.vm
    vm.$store.commit("app/SHOW_CONTACT_DIALOG")
    await localVue.nextTick()
    const QDialog = wrapper.findComponent({ name: "QDialog" })
    QDialog.vm.$listeners.hide()
    await localVue.nextTick()

    expect(vm.$store.state.app.displayContactDialog).toBe(false)
    expect(QDialog.props().value).toBe(false)
  })
})
