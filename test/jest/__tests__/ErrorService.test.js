import { createLocalVue } from "@vue/test-utils"
import Vuex from "vuex"
import ErrorService from "src/services/error-service"

const localVue = createLocalVue()
localVue.use(Vuex)

const app = {
  namespaced: true,
  state: () => ({
    displayErrorDialog: false,
    errorSummary: {
      code: 500,
      message: "General Test Error"
    }
  }),
  mutations: {
    SHOW_ERROR_DIALOG (state) {
      state.displayErrorDialog = true
    },
    CLOSE_ERROR_DIALOG (state) {
      state.displayErrorDialog = false
    },
    SET_ERROR (state, { error }) {
      state.errorSummary = {
        ...state.errorSummary,
        ...error
      }
    }
  }
}

function createStore (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      app
    }
  })

  return Store
}

describe("ErrorService", () => {
  it("sets 'show error dialog' to true", () => {
    const store = createStore()
    const errorService = new ErrorService(store)

    errorService.onError("Test Error")

    expect(store.state.app.displayErrorDialog).toBe(true)
  })
  it("sets proper error message", () => {
    const store = createStore()
    const errorService = new ErrorService(store)

    errorService.onError("Test Error")
    expect(store.state.app.errorSummary.message).toEqual("Test Error")
  })
})
