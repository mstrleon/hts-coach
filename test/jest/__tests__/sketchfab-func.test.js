/* eslint-disable jest/no-disabled-tests */
import SketchfabViewApiWrapper from "sketchfab-api-wrapper"
import SFInit, {
  exportedForTesting,
  switchColor,
  switchWood,
  switchColorOnCup,
  showTable,
  switchTray
} from "src/helpers/sketchfab-func"

jest.mock("sketchfab-api-wrapper")
let api = {}
const UID = "42"
beforeEach(async () => {
  jest.clearAllMocks()
  api = await SFInit(UID)
})

describe("SketchFab helper functions", () => {
  const testGroup = {
    component: "LayoutSelector",
    items: {
      1: {
        id: 1,
        title: "Layout 1",
        thumbnail: "icons/layout-1-1.png",
        node: "layout_1",
        annotation: "layout_1",
        uid: "testuid"
      }
    }
  }
  const testUid = "testuid"

  const testItem = {
    id: 5,
    title: "Standard",
    thumbnail: "icons/layout-2-1.png",
    node: "layout1",
    annotation: "layout_1",
    uid: "2bc87c7b40034b5396dc700e0bf0a3cc"
  }

  describe("SFInit function testing", () => {
    it("Calls SketchfabViewApiWrapper and passes uid", () => {
      expect(SketchfabViewApiWrapper).toBeCalledTimes(1)
      expect(api.uid).toBe(UID)
    })

    it("Calls init function", () => {
      expect(api.init).toBeCalledTimes(1)
    })

    it("add sf property to window object", () => {
      expect(window.sf).toBeDefined()
    })

    it("throws an Error when any error on SK appearing", async () => {
      SketchfabViewApiWrapper.mockImplementationOnce(({ uid }) => {
        throw Error()
      })
      let msg = ""
      try {
        api = await SFInit(UID)
      } catch (e) {
        msg = e.message
      }
      expect(msg).toBe("Could not create SketchFab API instance")
    })

    it("throws an Error if init trew an error", async () => {
      SketchfabViewApiWrapper.mockImplementationOnce(({ uid }) => {
        return {
          init () {
            throw Error()
          },
          uid
        }
      })
      let msg = ""
      try {
        api = await SFInit(UID)
      } catch (e) {
        msg = e.message
      }
      expect(msg).toBe("Could not init SketchFab API")
    })
  })

  describe("hideLayout", () => {
    it("hides proper layout with sf.hideNode", () => {
      exportedForTesting.hideLayout(testGroup, testUid)
      expect(api.hideNode).toBeCalledWith(testGroup.items[1].node)
    })
  })

  describe("loadLayout", () => {
    it("shows proper layout with sf.showNode", () => {
      exportedForTesting.loadLayout({ node: "test" })
      expect(api.showNode).toBeCalledWith("test")
    })
    it("directs to proper annotation with sf.gotoAnnotation and sf._getAnnotationByName", () => {
      exportedForTesting.loadLayout({ node: "test", annotation: "2" })
      expect(api._getAnnotationByName).toBeCalledWith("2")
      expect(api.gotoAnnotation).toBeCalledWith(2)
    })
  })

  describe("switchColor", () => {
    let msg = ""
    const hexColor = "#cccccc"
    const param3d = {
      material: "m",
      specular: 11,
      glossiness: 22,
      cavity: 33,
      metalnessFactor: 44,
      normal: 55
    }
    beforeEach(() => {
      jest.clearAllMocks()
      msg = ""
    })
    afterEach(() => {})
    it("calls sf.hexToLiner with proper argument", () => {
      switchColor(hexColor, param3d)
      expect(api.hexToLiner).toBeCalledWith(hexColor)
    })
    it("calls sf._getMaterialByName with applied parameters", () => {
      switchColor(hexColor, param3d)
      expect(api._getMaterialByName).toBeCalledWith("m")
    })
    it("calls sf.setMaterial with applied parameters", () => {
      switchColor(hexColor, param3d)
      const expectedParams = {
        channels: {
          AlbedoPBR: { color: api.hexToLiner(hexColor) },
          GlossinessPBR: { factor: param3d.glossiness, enable: true },
          SpecularF0: { factor: param3d.specular },
          CavityPBR: { enable: true, texture: [] },
          NormalMap: { enable: true, texture: [] },
          MetalnessPBR: { factor: param3d.metalnessFactor },
          RoughnessPBR: {}
        }
      }
      expect(api.setMaterial).toBeCalledWith(expectedParams)
    })
    it("throws an error if unproper args given", () => {
      try {
        switchColor(hexColor, {})
      } catch (e) {
        msg = e.message
      }
      expect(msg).toBe("bad switch color parameters given")
    })
    it("throws an error if something went wrong", () => {
      api.setMaterial = jest.fn(() => {
        throw new Error()
      })
      try {
        switchColor(hexColor, param3d)
      } catch (e) {
        msg = e.message
      }
      expect(msg).toBe("could not apply new material Error")
    })
  })

  describe("switchWood", () => {
    const hexColor = "#cccccc"
    it("calls sf.hexToLiner with applied parameters", () => {
      switchWood(hexColor)
      expect(api.hexToLiner).toBeCalledWith(hexColor)
    })
    it("calls sf._getMaterialByName with wood_mat param", () => {
      switchWood(hexColor)
      expect(api._getMaterialByName).toBeCalledWith("wood_mat")
    })
    it("calls sf.setMaterial with proper param", () => {
      const expectedMat = {
        channels: {
          ...api._getMaterialByName().channels,
          AlbedoPBR: { color: 777 }
        }
      }
      switchWood(hexColor)
      expect(api.setMaterial).toBeCalledWith(expectedMat)
    })
  })
  describe("switchColorOnCup", () => {
    const param3d = {
      color: "#cccccc",
      metalness: 11,
      roughness: true,
      material: "test"
    }
    it("calls sf.hexToLiner with applied parameters", () => {
      switchColorOnCup(param3d)
      expect(api.hexToLiner).toBeCalledWith(param3d.color)
    })
    it("calls sf._getMaterialByName with applied parameters", () => {
      switchColorOnCup(param3d)
      expect(api._getMaterialByName).toBeCalledWith(param3d.material)
    })
    it("calls sf.setMaterial with proper param", () => {
      const expectedMat = {
        channels: {
          ...api._getMaterialByName().channels,
          AlbedoPBR: { color: 777 },
          MetalnessPBR: { factor: param3d.metalness },
          RoughnessPBR: { enable: param3d.roughness }
        }
      }
      switchColorOnCup(param3d)
      expect(api.setMaterial).toBeCalledWith(expectedMat)
    })
  })
  describe("showTable", () => {
    it("calls hideNode if false", () => {
      showTable(false)
      expect(api.hideNode).toBeCalledWith("table")
    })
    it("calls showNode if true", () => {
      showTable(true)
      expect(api.showNode).toBeCalledWith("table")
    })
  })
  describe("switchTray", () => {
    const param3d = "test"
    it("hides proper nodes", () => {
      switchTray(param3d)
      expect(api.hideNode).toHaveBeenCalledTimes(2)
      expect(api.hideNode.mock.calls[0][0]).toEqual("table_oval")
      expect(api.hideNode.mock.calls[1][0]).toEqual("table_rectangular")
    })
    it("calls proper showNode", () => {
      switchTray(param3d)
      expect(api.showNode).toHaveBeenCalledWith(param3d)
    })
  })
})
