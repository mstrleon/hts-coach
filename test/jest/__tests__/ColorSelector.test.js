import { mount, createLocalVue } from "@vue/test-utils"
import ColorSelector from "components/ItemPropSelectors/ColorSelector"
import * as All from "quasar"
const { Quasar } = All
import Vuex from "vuex"

const components = Object.keys(All).reduce((object, key) => {
  const val = All[key]
  if (val && val.component && val.component.name != null) {
    object[key] = val
  }
  return object
}, {})

const localVue = createLocalVue()
localVue.use(Quasar, { components, directives: { ClosePopup: All.ClosePopup } })
localVue.use(Vuex)

const app = {
  namespaced: true,
  state: () => ({
    selectedColor: {
      1: "#7B0016",
      2: "#090909"
    },
    colorText: {
      1: "Theater Red",
      2: "Black Onyx"
    },
    materialText: {
      1: "Cine Suede",
      2: "Silk Leather"
    },
    loaded: true
  }),
  mutations: {
    SET_COLOR (state, { color, text, material, section }) {
      state.selectedColor[section] = color
      state.colorText[section] = text
      state.materialText[section] = material
    }
  }
}

function createStore (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      app
    }
  })

  return Store
}

function factory () {
  const store = createStore()
  return mount(ColorSelector, {
    store,
    localVue,
    propsData: {
      item: "#61331a",
      name: "Red Test",
      group: {
        header: {
          title: "Silk Leather",
          subtitle: "Custom colors available upon request",
          tooltip:
            "Our most popular material - an extremely buttery soft, breathable, and flexible synthetic material that has the visual and spill resistant qualities of leather. This premium material carries a 10% surcharge over Valentino Leather.",
          image: "icons/step2/silk-leather.png"
        },
        component: "ColorSelector",
        section: 1,
        mobilePanel: 1,
        param3d: {
          material: "cloth_1_mat",
          specular: 0.3,
          glossiness: 0.8,
          normal: "silk_leather_normal_0.jpg",
          cavity: "silk_leather_cavity_0.jpg",
          metalnessFactor: 0
        },
        items: {
          Ash: "#47433e",
          Aztec: "#61331a",
          "Beet Root": "#431e1f",
          "Black Onyx": "#090909",
          Cabernet: "#281312",
          "French Vanilla": "#978264",
          Caramel: "#57301d",
          "Desert Clay": "#84654f",
          Indigo: "#1a1b2d",
          Truffle: "#453329",
          "Evening Blue": "#080e20",
          "Pompeian Red": "#5b0401",
          Shiitake: "#413735",
          Peppercorn: "#1e1513"
        }
      },
      mobile: false
    }
  })
}

const mockSwitchColor = jest.fn()
jest.mock("src/helpers/sketchfab-func", () => ({
  switchColor: () => mockSwitchColor()
}))

describe("Color Selector", () => {
  it("calls switchColor with apropriate params", () => {
    const wrapper = factory()
    const vm = wrapper.vm
    wrapper.trigger("click")
    expect(mockSwitchColor).toBeCalled()
  })
})
