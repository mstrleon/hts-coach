import { mount, createLocalVue } from "@vue/test-utils"
import TheErrorDialog from "components/Dialogs/TheErrorDialog"
import * as All from "quasar"
const { Quasar } = All
import Vuex from "vuex"

const components = Object.keys(All).reduce((object, key) => {
  const val = All[key]
  if (val && val.component && val.component.name != null) {
    object[key] = val
  }
  return object
}, {})

const localVue = createLocalVue()
localVue.use(Quasar, { components, directives: { ClosePopup: All.ClosePopup } })
localVue.use(Vuex)

const app = {
  namespaced: true,
  state: () => ({
    displayErrorDialog: false,
    errorSummary: {
      code: 500,
      message: "General Test Error"
    }
  }),
  mutations: {
    SHOW_ERROR_DIALOG (state) {
      state.displayErrorDialog = true
    },
    CLOSE_ERROR_DIALOG (state) {
      state.displayErrorDialog = false
    },
    SET_ERROR (state, { error }) {
      state.errorSummary = {
        ...state.errorSummary,
        ...error
      }
    }
  }
}

function createStore (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      app
    }
  })

  return Store
}

function factory () {
  const store = createStore()
  return mount(TheErrorDialog, {
    store,
    localVue
  })
}

describe("The Error Dialog", () => {
  it("Updates Qdialog value to true on vuex change", async () => {
    const wrapper = factory()
    const vm = wrapper.vm
    vm.$store.commit("app/SHOW_ERROR_DIALOG")
    const QDialog = wrapper.findComponent({ name: "QDialog" })
    await localVue.nextTick()

    expect(QDialog.props().value).toBe(true)
  })

  it("Displays a proper error message", async () => {
    const wrapper = factory()
    const vm = wrapper.vm
    vm.$store.commit("app/SHOW_ERROR_DIALOG")
    await localVue.nextTick()
    const errorMessage = wrapper.findComponent({ ref: "errorMessage" })

    expect(errorMessage.exists()).toBe(true)
    expect(errorMessage.html()).toContain("General Test Error")
  })

  it("Has a close button and it works", async () => {
    const wrapper = factory()
    const vm = wrapper.vm
    vm.$store.commit("app/SHOW_ERROR_DIALOG")
    await localVue.nextTick()
    const closeButton = wrapper.findComponent({ ref: "close" })

    expect(closeButton.exists()).toBe(true)

    closeButton.trigger("hide")
    await localVue.nextTick()
    const QDialog = wrapper.findComponent({ name: "QDialog" })
    expect(QDialog.props().value).toBe(false)
    expect(vm.$store.state.app.displayErrorDialog).toBe(false)
  })

  it("Updates vuex on hide event", async () => {
    const wrapper = factory()
    const vm = wrapper.vm
    vm.$store.commit("app/SHOW_ERROR_DIALOG")
    await localVue.nextTick()
    const QDialog = wrapper.findComponent({ name: "QDialog" })
    QDialog.vm.$listeners.hide()
    await localVue.nextTick()

    expect(vm.$store.state.app.displayErrorDialog).toBe(false)
    expect(QDialog.props().value).toBe(false)
  })
})
