## Testing SF functions is the most critical

### SF init:
  + calls SketchfabViewApiWrapper with needed params
  + catches and rethrows error "Could not create SketchFab API instance"
  + calls api init
  + catches and rethrows error "Could not init SketchFab API
  + adds api to window

### hideLayout
+ hides proper layout sf.hideNode

### loadLayout
+ loads proper layout with sf.showNode
+ directs to proper annotation with sf.gotoAnnotation and sf._getAnnotationByName
### switchColor
 + calls sf.setMaterial with applied parameters
 + calls sf.hexToLiner with proper argument
 + calls sf._getMaterialByName with applied parameters
 + throws an error if something went wrong
 + throws an error if unproper args given

### switchWood, switchColorOnCup
+ calls sf.setMaterial with applied parameters
+ calls sf._getMaterialByName with wood_mat param
+ calls sf.hexToLiner with applied parameters

### showTable
+ calls proper layout sf.hideNode if false
+ calls proper layout sf.hideNode if true

### switchTray
+ calls proper showNode
+ hides both nodes


# Components

## TheErrorDialog

+ text
+ closes
+ opens by vuex

## TheContactDialog

+ text
+ closes
+ opens by vuex

## Selectors
- tooltip if necesary
- proper onclick calls
- change color if color
- disabled class if proper data


## FormSubmit
- Calls emailservice
- does not call if not enough data
- updates sening/not sending
- calls errorservice on error

## YouHaveChosen
- renders default data


## QuanitySelector
- default 1
onclick
- updates vuex
- updates dom


## ErrorService
+ sets displayErrorDialog dialog to true
+ sets proper error message
